import React from 'react'
import Category from './category'
import './categories.css'
import SearchContainer from '../../widgets/containers/SearchContainer'

function Categories(props) {
  return(
    <div className="Categories">
      <SearchContainer />
      {
        props.Categories.map((item) => {
          return (
            <Category 
              key={item.id} 
              {...item}
              handleOpenModal = {props.handleOpenModal}
            />
          )
        })
      }
    </div>
  )
}

export default Categories