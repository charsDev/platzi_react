# Introducción
## __¿Por qué aprender React?__
* __React:__ Es una libreria de JS que nos ayuda a construir interfaces de usuario
* __Interfaz de usuario:__ Lo que hacemos con html, es el header, footer, contenido, navegacion, etc
* React es:
  * Declarativo: es muy sencillo y de leer interfaces
  * Basado en componentes: como si fuera un juego de Jenga o Legos. Varios componentes forman algo
  * Aprende una vez, escribe donde quieras: aprendes algo y puedes ir actualizando por pedazos
* React es una libreria que solo se encarga de construir interfaces, hay otras librerias que se encargan de otras cosas:
  * React Router: sirve para enlazar diferentes rutas,hacer una SPA
  * Redux:manejar el estado global de la aplicacion
  * React Native: hacer aplicaciones moviles con react
  * Nextjs: funciona como un framework, renderear los compenentes de react y entregarlo como un html puro
* Facebook usa y actualiza react

# Preparando el entorno
## __Requisitos previos del curso__
* Instalar node.js
* Existen dos maneras de empezar con react
  * [Facebook Incubator App](https://github.com/facebook/create-react-app "Facebook incubator") es un boilerplater para empezar con react sin necesidad de meterse a mucha configuracion
* Creamos una aplicacion sencilla, solo npm start dentro de la carpeta y funciona, nada que complicarse. Solo seguir pasos de la pagina

## __Configuración de Webpack para React__
* Hacer una configuracion custon con webpack es la 2da forma de empezar con React
* Este tipo de configuraciones se recomiendan para proyectos grandes, donde uno quiere tener el control de todo. La anterior es mas para proyectos rapidos, pruebas y cosas sencillas
* Creamos carpeta platzi-video, que es donde estara nuestro proyecto
* Dentro creamos dos archivos (los trajimos del curso de Webpack):
  * __webpack.config.js:__ contiene la configuracion general
  * __webpack.dev.config.js:__ contiene configuracion para desarrollo
* Solo le cambiamos el nombre al entry (invie -> platzi-video) en los dos archivos
* Instalamos las dependencias (igual traidas de webpack)
* Creamos los scripts ](build:dev, build, build:local, b uild:prod)
* Creamos nuestro archivo index.js y pasamos un console.log
* Creamos un index.html y linkeamos el archivo js mediante la etiquete script
* Corremos la tarea build:prod y se crea nuestra primera carpeta dist con la carpeta js y el archivo platzi-vide.js, como lo declaramos en el webpack.config

# Bases de React
## __Usando ReactDOM y JSX 1ra parte__
* Instalamos dependencias de react@16.1.1 y react-dom@16.1.1 y las agregamos a las dependencias del proyeto (--save)
  * __React:__  sirve para crear los componentens (las piezas de lego)
  * __React-Dom:__ sirve para poner los componentes en algun lugar. Renderizarlos, en este caso dentro del navegador. Tiene un metodo llamado *render* que recibe dos parametros, que voy a renderizar (componente o elemento de react), donde lo hare (lugar en el dom)
* Un componente es un conjunto de elementos de react
* En el archivo index.html agregamos un div#app en el cual se renderizara nuestros elementos o componentes
* En el archivo index.js creamos dos constantes:
  * app: obtiene el elemento del documento index.html con el id = app
  * holaMundo: contiene el elemento que vamos a renderizar
* Estas constantes se las pasamos al ReactDom.render
* En el index.html cambiamos la ruta del script, ya que utilizaremos el script build:dev y asi evitarnos estar cambiando el nombre del archivo (hashes) cada que actualicemos con build:prod

## __Creando componentes en ReactJS__
* Creamos carpeta src, dentro otra que se llama playlist y dentro otra con el nombre de components
* Dentro de la carpeta components creamos el archivo media.js (el cual es un componente minimo para ver videos)
* importamos React y Component (destructuring) de react y creamos la clase Media la cual extiende de Component
* Component tiene un metodo principal __render__ tiene dentro el codigo que va a ser como el html que va tener el componente
* Lo que parece html dentro del metodo render es JSX (Sintaxis para construir elementos dentro de ReactJS)
* Exportamos nuestro componente (export.deafult)
* Lo importamos en el index.js (Media) y se lo pasamos al ReactDom.render
* Los componentes empiezan en mayusculas, es la convencion
* En el archivo index.js cambiamos la manera de llamar el render, igual usamos destructuring para nada mas llamarlo desde react-dom

## __Estilos en ReactJS__
#### __Estilos en linea__
* Los elementos en JSX tambien tienen el atributo style, lo usamos en el div padre (media.js)
* Los estilos se crean como si fueran objetos (JSON) en la constante styles
* En el caso de atributos que se compoen de dos palabras en css ejemplo font-size, en JS se usa el camelCase ejemplo fontSize
* Las unidades de medida por default son pixeles, asi que pasando solo el valor sin la unidad, js le pone la terminacion px, en caso de querer usar otra es necesario encerrarlo en comillas '1em' 
#### __Estilos archivo externo .CSS__
* Creamos archivo media.css
* Importamos el archivo media.css en el media.js
* Como en react no podemos usar la palabra class, usamos la palabra className para pasar las clases

## __Propiedades en ReactJS__
* A lo que nosotros conocemos como atributos (src, alt, width) en css en __*react*__ se le conoce como __*atributos*__
* Las propiedades se las podemos pasar al componente, como cuando es contenido dinamico, pueda ir cambiando el contenido y asi construir diferentes componentes
* En el archivo index.js en la parte del render, en el Media le pasamos las propiedades que recibira nuestro archivo media.js en la clase Media
* En el archivo media.js recibimos las propiedades y las utilizamos para cambiar los valores de image, title, author
* Estas propiedades van dentro de las __{}__ y se reciben meidante un __this.props__, que son las propiedades que recibe la clase Media
* Ejemplo de como se puede utilizar destucting __*(const {title, author, image})*__ para obtener las propiedades en una sola llamada a thsi.props. [Destructing this.props](http://s3.amazonaws.com/citydrive/img/this-props.png "Destructing this.props")

## __Validando tipado en propiedades__
* ¿Que tal si el componente le llegan los datos de las propiedades mal. Eso puede romper la aplicación o no puede verse como tu deseas
* Instalamos dependecia prop-types (para este curso @15.6.0)
* Importamos en PropTypes la dependencia prop-types
* Llamamos a propTypes de Media (Media.propTypes), propTypes es un atributo de la clase del component de react (Media)
* Y dentro le pasamos los nombres de las props (image, title, author) y, ahora si, con PropTypes validamos que sean del valor correcto
* Los tipos de PropTypes (dependecia) que hay son:
  * func - Funcion
  * string - Texto
  * object - Objeto
  * bool - Booleano
  * number - Número
  * array - Array
  * [Pagina para saber mas de PropTypes](https://reactjs.org/docs/typechecking-with-proptypes.html "PropTypes")
* En el caso de la propiedad __*type*__ esta nos sirve para identificar el tipo de propiedad que se esta pasando, en este caso sea video o audio
* Para el caso de type usamos la funcion oneOf de PropTypes
* Utilizamos la propiedad .isRequired cuando necesitemos que una propiedad si o si sea pasada. Ejemplo title, si lo eliminamos marca error

## __Enlazando eventos del DOM__
* Como enlazar eventos con el dom
* Mediante las propiedades del componente podemos tambien enlazar eventos, en este caso un evento click (onClick)
* Al evento onClick le pasamos el nombre de la funcion que corresponde a ese evento (this.handleClick)
  * Utilizamos __this__ para llamar algo que esta dentro del scope de la case Media, que se le pasa por props en este caso
  * Por convencion el manejador del evento onClick se le da el nombre de handleClick, pero se puede utilizar cualquier nombre
* Pagina para saber mas de handling-events [React Handling Events](https://reactjs.org/docs/handling-events.html "React Handling-Events")
#### __Forma ES6__
* Como deseamos que nuestro manejador de evento(handleClick) pueda hacer uso de los props que recibe el componente de la clase Media, necesitamos crear un constructor
* Un constructor, como su nombre lo indica, se encarga de construir todo antes de que el componente sea rendereado, por eso recibe como parametro las propiedades (props) que le mande el archivo index.js
* Luego le pasamos las propiedades (props) como parametros al metodo super(), el cual se encarga de agregar las props al entorno del componente con la clase Media __this__ y asi poder usar las props en el
* Bindeamos la funcion handleClick con el __this__
* Todo esto para que la funcion handleClick pueda recibir las propiedades del componente __(this.props)__
#### __Forma ES7__
* Gracias a las arrows functions ya no tenemos que bindear como en ES6, ay que las arrow functions heredan el contexto (this) de su padre, en este caso el componente de la clase Media
* Cambia un poco la forma de la funcion handleClik pero se ahorra uno mucho trabajo y codigo
* Todo esto lo podemos usar gracias a que en nuestros archivos de configuracion  __webpack.config__ en el loader babel-loader tenemos habilitado el __stage-2__ que es el mas cercano a ES7 y asi poder usar las arrow functions

## __Estado de los componentes en ReactJs__
* Las propiedades en reac son inmutables por lo tanto no pueden ser dinamicas, para solucionar esto existen los estados
* Los estados si son mutables y pueden tener valores dinamicos
#### __Forma ES6__
* En el constructor, despues de super(props), asignamos el valor de __props.author__ en el __this.state__ , esto para recibir el primer author
* Luego en la funcion __handleClick__ seteamos el state __setState__ con el nuevo contenido de author
#### __Forma ES7__
* Al __state__ le asignamos el contendio de author
* Seteamos el contenido del author con __setState__ en la funcion handleClick

## __Ciclo de vida de los componentes__
* Los componentes tienen un ciclo de vida, que es basicamente como funcionan, como entran en escena, como se van de escena, actualizacion
* El ciclo de vida se divide en 3 componentes:
  * __Montado:__ Cuando vamos a montar un componente, es decir que va a entrar en escena.
    * __Constructor:__ Metodo llamado antes de que el componente sea montado  
      1. Podemos iniciar estados  
      2. Enlazar (bind) de eventos  
      3. Es el primer metodo que se llama al iniciar un componente
    * __componentWillMount:__ Metodo llamado inmediatamente antes de que el componente se vaya a montar (el componente aun no se ve)
      1. Puede hacer un setState()
      2. No hacer llamados a un API o suscripcion a eventos
    * __render__ Contiene todos los elementos a renderizar (estructura del componente)
      1. Contiene jsx en el return
      2. Puedes calcular propiedades *nCompleto = name + lastName*
    * __componentDidMount:__ Metodo llamado luego de montarse el componente (el componente ya esta en pantalla)
      1. Solo se lanza una vez
      2. Enlazar (bind) de eventos
      3. Es el primer metodo que se llama al instanciar un componente
      4. Hacer llamados a un API o suscripcion a eventos

  * __Actualizacion:__ Cuando le llegan nuevas propiedades al componente
    * __componentWillReceiveProps:__ El metod recibira nuevas propiedas
      1. Metodo llamado al recibir nuevas propiedades
      2. Sirve para actulizar el estado (state) con base en las nuevas propiedades
    * __shouldComponentUpdate:__ El encargado de verificar que las propiedades que le lleguen al componente no sean las mismas que ya estan seteadas
      1. Metodo que condiciona si el componente se debe volver a renderizar
      2. Utilizado para optimizar el rendimiento
    * __componentWillUpdate:__ Si ya paso *shouldComponentUpdate* y recibio un true de que la info es diferente a la que se tiene, entonces se debe actualizar
      1. Metodo llamado antes de re-renderizar un componente
      2. Utilizado para optimizar el rendimiento
    * __render__
      1. re-render
    * __componentDidUpdate:__ El componente ya se actualizo
      1. Metodo llamado luego del re-render

  * __Desmontado:__ Como este componente se va de la escena
    * __componentWillUnmount:__ Metodo que se va lanzar antes de que el componente se vaya de la escena
      1. Metodo llamado antes de que el componente sea retirado de la aplicacion

  * __Manejo de errors (desde react 16):__ Nos ayuda a prevenir que nuestra aplicacion se rompa si algunos de nuestros componentes tiene errores
    * __componentDidCatch:__ Cuando encuentro un error que voy a hacer, no previene actua si encuentra errores
      1. Si ocurre algun error al renderizar el componente este metodo es invocado
      2. El manejo de errores solo ocurre en componentes hijos

# Trabajando con componentes en React
## __Listas en ReactJS__
* ¿Como rendereamos multiples elementos apartir de multiples datos?
* Ya no vamos a mandar propiedades hardcodeadas en el render del index.js
* __Listas:__ arrays o arreglos dentro de programacion
* Agregamos el archivo api.json y lo llamamos desde index.js
* __Debido a que en el archivo index.js no pondremos toda la logica de nuestros componentes, es por eso que creamos otro componente playlist.js el cual contiene la logica para recorrer cada uno de los elementos del api.json y le devolvera al componente Media cada uno de estos para que los pueda renderear__
* En el archivo index.js:
  * Importamos nuestro componente Playlist
  * Importamos como data el contendio del archivo api.json
  * Eliminamos componente Media de los imports
  * En el render eliminamos las propiedades hardcodeadas que le pasabamos al componente y ahora pasamos la data
  * En el render el componente ya no es Media ahora es Playlist
* En el archivo playlist.js 
  * Importaremos react,  Media y playlist.css
  * Creamos clase Playlist y declaramos una conts __playlist__ que recibe la data que mandamos como propiedades en el archivo index.js (render)
  * Creamos un div con la className Playlist (para el css) y recorremos el valor de __playlist__ con el atributo map 
  * Le pasamos las propiedades al componente Media usando los spread operators (...) recorren los elementos del elemto que se pase por parametro (es6) 
  * exportamos Playlist
* En el archivo media.js:
  * Cambiamos el images por cover
  * En el Media-author, cambiamos state por props

## __Componentes puros y funcionales en ReactJS__
* Hay dos formas de hacer componentes en React: puros y funcionales
  * Componentes Puros (PureComponent) extienden de PureComponent en lugar de Component 
  * Componentes Funcionales (Component) en lugar de clases seran funciones, no tiene ciclo de vida
* Algo que diferencia entre estos dos componentes es que el PureComponent ya tiene integrado la parte del ciclo de vide de un componente __shouldComponentUpdate__, quiere decir que sabra cuando actualizarse
* Si mi componente no tiene un cliclo de vida lo mejor es usar componente funcional
* Modificamos media.js para que sea un __componente puro__ cambiamos Component por PureComponent
* Modificamos playlist.js en un __componente funcional__ , quitamos el render, los this y dejamos de importar {Component}

## __Smart Components and Dumb Components__
* Como se ve (Dumb, Pure, Skinny, Presentational Component) VS Como se hace (Smart, StateFull, Fat, Container)
* ¿Por que es importante separar los componentes Dumb de los Smart:
  * Separacion de responsalbilidades (como se ve y como funciona)
  * Mejora la capacidad de reutilizar componentes 
* Presentational(Dumb) - Como se ve :
  * Puede contener smart components u otros componentes de UI (Cualquier componente)
  * Permiten composición con {props.children} (Poner componente dentro de otro)
  * No depeden del resto de la aplicación (Tan sencillo que puede ser reutilizado)
  * No especifica cómo los datos son cargados o mutados (no hay state, recibe propiedades puras)
  * Recibe datos y callbacks solo con propiedades 
  * Rara vez tienen su propio estado (evitar tener state tambien ciclo de vida)
  * Están escritos como componentes funcionales (Componente Funcional) a menos que necesiten mejoras de performance (PureComponent). 
* Containers(Smart) - Que hace
  * Concetrado en el funcionamiento de la aplicación (Manager de la aplicacion)
  * Contienen componentes de UI u otros containers 
  * No tienen estilos (css)
  * Proveen de datos a componentes de UI u otros contenedores (pasa datos al componente dumb)
  * Proveen de callbacks a la UI (eventos del dumb los tiene el inteligente)
  * Normalmente tienen estado
  * Llaman acciones
  * Generados por higher order components

## __Composición de componentes__
* Creamos la carpeta icons la cual contrendra dentro la carpeta components
* Apartir de ahora cuando creemos carpeta __components__ contendra los componentes puros o funcionales
* Y cuando tengamos una carpeta con el nombre __container__ contendra los compenentes con estado (dumb) o componentes inteligentes
* Creamos un componente icon.js dentro de la carpeta icons/components
  * Dentro declaramos una funcion Icon la cual recibe unas propiedades (del archivo play.js), hacemos destructuring para tomar los valores color y size de las props.
  * Dentro de la etiqueta svg declaramos los atributos y sus valores (size y color)
  * Dentro de las etiquetas svg´s pasamos el valor prop.children, el cual contiene las propiedades de la etiqueta svg, esto lo hacemos con el atributo __path d__ declarado en el archivo play.js
* Dentro de la carpeta __components__ creamos el componente play.js el cual contendra el icono de play
  * Creamos la funcion Play, le pasamos las props (size, color) que vienen de playlist.js
  * En el return llamamos al componente Icon y le pasamos dichas props
  * Dentro de las etiquetas Icon pasamos la etiqueta __path d__ (svg), esto para que en el archivo icon.js podamos usar las props.children
* Dentro del componente playlist.js importamos el componente Play (play.js) y le pasamos los atributos size y color.
* Como podemos ver en este capitulo entendimos la composicion de componentes de la siguiente manera:
  1. Creamos el componente icon.js el cual contiene a grandes rasgos la estructura del svg
  2. Creamos el componente play.js el cual llama al componente Icon pasandole las props
  3. Y por ultimo en el componente playlist.js llamamos al componente Play(play.js)

## __Estructura del proyecto con Containers y UI components: layout de nuestro sitio web__
* En nuestros archivos config cambiamos el entry:
  * Le asignamos el nombre de __home__
  * Cambiamos la ruta hacia el archivo src/entries/home.js
* Cambiamos la ruta del script para que ahora apunte al archivo js/home.js 
* Creamos una carpeta entries, en la cual se almacenara el componente el cual manejara toda la UI de dicho componente
  * Dentro creamos el componente home.js el cual importara el componente __pages/containers/home(smart)__
  * Rendeara el componente importado
* Creamos una carpeta pages y dentro dos carpetas mas __components__ y __containers__
  * Como vimos en el tema anterior en los components iran los componetes puros o funcionales y en la carpeta de containers iran los componentes smart o dumb
  * En la carpeta __components__ creamos el componente funcional HomeLayout(home-layout.js) 
    * Retornamos las props.children y en este caso un texto para ver que si funciona
  * En la carpeta __containers__ creamos el componente intengligente Home(home.js)
    * Importamos el componente HomeLayout
    * Retornamos dicho componente

## __Estructura del proyecto con Containers y UI components: Categorías__
* Le pasamos las props de __data__ al componente __entries/home.js__
* Creamos una carpeta llamada __categories__ dentro de la cual estara otra carpeta __components__ y dentro de esta dos archivos:
  * __categories.js__ creamos un componente funcional Categories
    * Recibe las props y retorna la iteracion de cada una de los items de las propiedades
    * Dentro de la iteracion retornamos el componente Category y le pasamos la key y un spread operator (...item) con los items
  * __category.js__ en este componente creamos la funcion Category 
    * Recibe las props y renderiza description, title y el componente Playlist dentro del cual pasamos la propiedad playlist
  * Dentro del componente playlist.js refactorizamos el codigo
  * En el componente containers/home.js llamamos al componente Categories y le pasamos las props
  * En el componente entries/home.js rendereamos el componente Home y le pasamos las props que es la data que apunta al archi api.json

## __Estructura del proyecto con Containers y UI components: Contenido relacionado y estilos__
* Creamos el componente __pages/components/related.js__:
  * Importamos el logo y los estilos del componente
  * Retornamos un div con la clase Related y una imagen con su ancho
* En el component __container/home.js__ importamos el componente Related y lo utilizamos dentro del componente __HomeLayout__
* Agregamos estilos a los componentes:
  * home-layout
  * categories
  * category

## __Portales__
* Los __portales__ son la forma en que React(16) puede renderizar otros componentes que no vivan dentro de la etiqueta app en el index.html, por ejemplo los modales, no se renderizan dentro de la etiqueta con el __id home-container__, si no que tendran su propia etiqueta con su __id modal-container__
* En el __index_html__
  * Creamos otra equtiqueta con el id modal-container
  * Renomanbramos la etiquera principal (app) por home-container
* En el componente __entries/home.js__:
  * Renombramos la const app por homeContainer
  * Cambiamos el nombre de la etiqueta en el render tambien
* Creamos una carpeta __widgets__
  * Creamos una carpeta __containers__ ya que nuestro modlaes seran inteligentes
  * Creamos el componente __modal.js__ 
    * Creamos la clase ModalContainer
    * Importamos createPortal, es la forma en que react crea los portales
    * Retornamos createPortal que recibe como parametros:
      * Lo que se va a pasar (informacion)
      * Donde se va a renderizars
* En el componente __container/home.js__
  * Importamos el componente ModalContainer
  * Dentro del componente HomeLayout agregamos el componente ModalContainer y dentro de este agregamos el contenido del componente, el cual se refiere en modal.js como los props.children

## __Modal__
* En el archivo __pages/containers/home.js__
  * Seteamos el state con el valor modalVisible = false
  * Creamos 2 funciones para setear el state:
    * handleOpenModal: mostrara el modal
    * handleCloseModal: cerrara el modal
  * En el componente __Categories__ pasamos como props la funcion handleOpenModal
  * Creamos una condicion ternaria (if) donde confirmamos que modalVisible = true entonces (&&) entra al ModalContainer
  * Dentro de este, creamos otro componente Modal el cual recibe como props el handleClick(funcion) el cual es igual al andleCloseModal
* Creamos el componente __widgets/components/modals.js__:
  * Creamos el componente funcional Modal, pasamos props
  * Retornamos una etiqueta con la clase Modal 
  * Dentro pasamos las props.children y creamos un boton el cual contiene el evento onClick que es igual a la props.handleClick (funcion). Este boton cierra el modal y solo se muestra cuando abre el modal
* En el componente __categories/components/categories.js__
  * En el componente Category pasamos como props el handleOpanModal que es igual a handleOpenModal(funcion)
* En el componente __categories/components/category.js__
  * En el componente Playlist pasamos como props el handleOpanModal que es igual a handleOpenModal(funcion)
* En el componente __playlist/components/playlist.js__
  * En el componente Media pasamos como props el handleClick que es igual a handleOpenModal(funcion)
* En el componente __playlist/components/media.js__
  * En la etiqueta con la clsae Media tenemos el evento onClick el cual recibie la funcion pasada por la props.handleClick

## __Manejo de errores__
* Desde React 16 existe una manera de manejar los errores, por si llegara a fallar nuestra app
* Esto se utiliza dentro de la parte del ciclo de vida __componentDidCatch__
* __componentDidCatch__ entrega dos parametros, el error y la info acerca del error
* Creamos el componente __error/containers/handle-error.js__
  * Creamos la clase __HandleError__
  * Seteamos el __state__ del error igual a false
  * Llamamos al componente del ciclo de vida __componentDidCatch__
  * Dentro del render reamos una condicional que si el state __handleError__ es true(existe error), entonces retornara el componente __RegularError__ 
* Creamos el componente funcional __error/components/regular-error.js__
  * Creamos la funcional __RegularError__ la cual retorna una etiqueta h1 con el menaje que deseamos que aparezca
* En el componente __paige/containers/home.js__
  * Importamos el compoente __HandleError__
  * Usamos dicho componente dentro del return
  * Envuelve a __HomeLayout__ ya que primero verificara que no existan errores, de ser asi solo mostrara el mensaje de error

## __UI de Formularios__
* El formulario estara dentro de la columna de categories, entonces ahi estara el formulario
* En el componente funcional __categories.js__
  * Importamos el componente inteligente __SearchContainer__
  * Despues de la etiqueta con la clase __Categories__ llamamos al componente __SearchContainer__
* Creamos componente inteligente __SearchContainer.js__
  * Importamos el componente funcional __Search__
  * Creamos la clase __SearchContainer__
  * Dentro retornamos el componente __Search__
* Creamos el componente funcional __search.js__
  * Importamos estilos __search.css__
  * Declaramos una constante __Search__ que contendra una arrow function
    * En esta arrow function usraremos los () en lugar de {}, de esta manera retornamos el JSX sin necesidad de utilizar la palabra __return__
    * Retornamos una etiqueta __form__ y una etiqueta __input__ con sus atributos y clases

## __Referencias a Eventos de HTML y Formularios__
* Al querer hacer una busqueda en el formulario la pagina se refresca, eso es de html ya que los datos que esten en el input se mandan por el metodo POST y traen los datos por un metodo GET, por eso el simbolo ? en la barra de direccion
* Si le agregamos el atributo __name__ al input, en la barra de direccion se nos mostrara despues del localhost/?/ y lo que le pasemos por name
* Si queremos evitar que eso ocurra lo hacemos por JS
* El evento que se lanza cuando se hace la busqueda se llama __submit__
* En el componente __search.js__
  * Recordando la manera de manejar los eventos en react es __on__ y el nombre del evento __submit__. Por lo tanto __onSubit__ seria el nombre del evento en react.
  * Al __onSubmit__ le pasamos el manejador de eventos __handleSubmit__ que viene de las props que manda el componente __SearchContainer.js__
  * En el input utilizamos el atributo __ref__ de la etiqueta para obtener el value del input
  * Si queremos setear un valor por defecto en el input, que afectara la busqueda, usamos el atributo de la etiqueta __defaultValue__ 
  * __Si queremos que el value del input venga con un formato especial, por ejemplo que lo que introduzcamos en vez de espacios tenga guiones lo hacemos de la siguiente forma__
    * Seteamos en value que sera igual al value que venga por props
    * El evento onChange apuntara a la funcion handleChange que viene poer las props
* En el componente __SearchContainer.js__ 
  * En el componente __Search__ mandamos como propiedad el __handleSubmit__
  * Creamos la funcion (con una arrow function) __handleSubmit__ y llamamos al metodo event.preventDefault el cual evita que la pagina se recargue
  * En el __handleSubmit__ imprimimos en consola el valor del value, que viene del componenten __search.js__
  * __Si queremos que el value del input venga con un formato especial, por ejemplo que lo que introduzcamos en vez de espacios tenga guiones lo hacemos de la siguiente forma__
    * En el componente __Search__ pasamos las props handleChange que es igual a la funcion __handleInputChange__ y el __value__ que sera igual al valor del state del value
    * En la funcion __handleInputChange__ seteamos el state que sera igual al valor del value formateado para que remplace los espacios por guiones

# Creando un reproductor de video
## __Iniciando un reproductor de video__
* Vamos a crear un reproductor de video dentro del __modal__ que hicimos en el capitulo de __portal__
* El reproductor de video maneja muchos eventos en el dom, mucha UI y es divertido
* El reproducto manejara un gran estado(state) y cambios, tendra un __container__ y un __component__ de UI 
* Creamos la __carpeta player__ y dentro de ella creamos las carpetas containers y components
  * En la __carpeta containers__
    * Creamos el componente smart __video-player.js__
    * Importamos el componente funcional __VideoPlayerLayout__
    * Creamos la clase VideoPlayer del componente
    * Retornamos el componente __VideoPlayerLayout__ 
      * Dentro llamamos a la etiqueta __video__ y le pasamos sus atributos
  * En la __carpeta components__
    * Creamos el componente funcional __video_player_layout.js__
    * Importamos los estilos correspondientes al componente
    * Creamos la constante __VideoPlayerLayout__ que sera igual a una arrow funtion que recibe las props
    * Dentro de la constante pasamos una etiqueta con la clase __VideoPlayer__ y dentro pasamos los __props.children__
* En la carpeta __pages/containers/home.js__
  * Importamos el componente inteligente __VideoPlayer__
  * Agregamos al return el componente __VideoPlayer__
* En el archivo __webpack.dev.config.js__
  * Agreagamos la linea __devtool: eval-source-map__ la cual nos sirve para que cuando tengamos un error, en la consola podamos visualizar en que archivo es el error

## __Componentes de Título y Fuente de Vídeo__
* En el componente __video-player.js__
  * Importamos los componetens __Video__ y __Title__
  * Rendereamos el componente __Title__ y le pasamos la propiedad de title
  * Eliminamos la etiqueta __video__ ya que no queremos los atributos hardcodeados y los haremos que sean dinamicos
  * Rendereamos el componente __Video__ y le pasamos las propiedades __autoPlay__ y __src__
* Creamos el componente inteligente __video.js__
  * Este componente se agrego a la carpeta components, ya que es si sera un componete de UI pero tambien tendra states, por lo cual es necesario utilizar una clase para extender de Component y asi usar los states
  * Importamos estilos
  * Creamos el componente con la clase __Video__
  * Retornamos una etiqueta con la clase Video
    * Dentro la etiquera __video__ con sus atributos los cuales obtendran sus valores de las propiedades que le pasaron al componente de donde fue llamado
* Creamos el componente funcional __title.js__
  * Importamos los estilos
  * Creamos una const __Title__ la cual sera una arrow funnction que recibira las props como argumentos
  * Dentro creamos una etiqueta con la clase __Title__
    * Creamos una etiqueta __h2__ y dentro pasamos el __props.title__

## __Play/Pausa:Creando la UI__
* En el componente __video-player.js__
  * Importamos el componente funcional __PlayPause__
  * Agregamos un __state__ que contiene el valor de __pause__ que sera igual a true
  * Creamos una arrow function __togglePlay__ la cual cada vez que se utilice cambiara el valor del state de __pause__, si es true a false y viceversa
  * Retornamos el componente __PlayPause__ y le pasamos los atributos __pause__ que es el state y __handleClick__ que se refier a ala funcion __togglePlay__
* Creamos el componente funcional __play-pause.js__
  * Importamos los componentes Play, Pause y los estilos
  * Creamos la constante PlayPause que es una arrow function que recibe las props
    * Pasamos la etiqueta con la clase __PlayPause__
    * Creamos un __operador condicional ternario(?:)__ 
      * Si __pause__ es true se mostrara el componente __Play__
      * Si __pause__ es false se mostrara el componente __Pause__
      * Ambos botones usan el manejador de eventos __onClick__ que recibe por propiedades la funcion handleClick

## __Agregando lógica al botón de Play y Pausa__
* En el componente __pages\container\home.js__
  * Al componente __VideoPlayer__ le pasamos la propiedad de autoplay sea igual a true
* En el componente __video-player.js__
  * Utilizamos al componente del ciclo de vida  __componentDidMount__, este componente se ejecuta despues del que componente cargue totalmente
    * Seteamos el state con un operador ternario, en el que el valor de __pause__ cambiara, si la propiedad autoplay es true la cambie a false y viceversa
  * En el componente __Video__ 
    * En la propiedad autoplay le pasamos el valor de props.autoplay
    * Agregamos la propiedad __pause__ con el valor del state de __pause__
* En el componente __video.js__
  * En la etiqueta de __video__ agregamos el atributo __ref__ que hace referencia a la funcion __setRef__
  * Declaramos la funcion __setRef__ la cual recibe como parametro element (element es el objeto del video que contiene los metodos para hacer play y pause) y se lo asigna a la variable  __this.video__ 
  * Utilizamos el ciclo __componentWillReceiveProps__ el cual nos sirve para saber si debemos actualizar algo, mediante las nextProps. 
    * Declaramos un condicional if en el cual compararemos si la nextProps.pause (la nueva por renderizar) es diferente de props.pause (que esta en el renderizado actual)
    * Si la condicion es cierta llama al metodo __togglePlay__ 
  * Creamos la funcion __togglePlay__ 
    * Con un condicional if comparamos si la props.pause es igual a true llama al metodo __this.video_play()__
    * De no ser asi entonces llamaremos al metodo  __this.video.pause()__

## __Duración del video__
* El video dentro de sus metadatos tiene la duracion del video y el tiempo trascurrido
* Estos datos los vamos a cargar mediante los [Media Events](https://reactjs.org/docs/events.html) en este caso usaremos el onLoadedMetadata
* En el componente __video-player.js__
  * Importamos componentes __Timer__ y __Controls__
  * Le pasamos al state un nuevo estado llamado __duration__ con valor de 0
  * Creamos la arroe function __handleLoadedMetadata__
    * Recibe como parametros el __event__
    * Declaramos __this.video__ que sea igual al __event.target__ elc ual trae la info y metadatos del video
    * Seteamos el __state duration__ para que sea igual __this.video-duration__ la duracion del video
  * Llamamos al componente __Controls__ 
    * Dentro de este componente estaran los componentes __PlayPause__ y __Timer__
  * Agregamos el componente __Timer__
    * Recibe como parametro la propiedad __duration__ que sera igual a __this.state.duration__
  * En el componente __Video__ agregamos una nueva propiedad __handleLoadedMetadata__ que sera igual a __this.handleLoadedMetadata__ la cual apunta a la funcion handleLoadedMetadata que declaramos arriba
* Creamos el componente __video-player-controls.js__
  * Este es un componente de UI para poder encapsular los controles del video
  * Importamos sus estilos
* En el componente __video.js__ 
  * Declaramos una constante __handleLoadedMetadata__ la cual obtiene su valor de las propiedades __this.props__ __(DESTRUCTURING)__
  * En la etiqueta video pasamos el __onLoadedMetadata__ que es un Media Event para jsx y le damos el valor de la constante __handleLoadedMetadata__
* Creamos el componente __timer.js__
  * Importamos sus estilos
  * Declaramos la constante Timer que es igual a una arrow function
    * Pasamos una etiqueta con la clase __Timer__
    * Dentro pasamos la estructura html junto con la propiedad __{props.duration}__

## __Tiempo transcurrido__
* En el componente __video-player.js__
  * Importamos el archivo __formattedTime__ el cual sirve para darle formato a los numeros del tiempo actual del video
  * Agregamos dos estados mas, duration y currenTime
  * En la funcion __handleLoadedMetadata__
    * Formateamos el valor del setState __duration__
  * En la funcion __handleTimeUpdate__
    * Formateamos el valor del setState __currentTime__
  * En el componente __Timer__
    * Agregamos el atributo __currentTime__ el cual pasa el valor del tiempo en el que va el video
  * En el componente __Video__
    * Le pasamos el atributo __handleTimeUpdate__ que es igual a la funcion handleTimeUpdate
* En el componente __video.js__
  * Agregamos la variable __handleTimeUpdate__ (DESTRUCTURING) la cual sera igual a la funcion con el mismo nombre que paso como props del componente video-player.js
  * En la etiqueta __video__ agregamos el __Media Event__ con el nombre __onTimeUpdate__ el cual se encarga de que cada que el tiempo se actualice haga algo, en este caso, llama a la variable __handleTimeUpdate__
* En el componente __timer.js__
  * Modificamos la etiqueta span para que por las props reciba el __currentTime__ y el __duration__ previamente formateados
* Creamos el archivo __formatted-time.js__
  * Declaramos la arrow function __leftPad__
    * Recibe un number(string) como parametro
    * Creamos la constante __pad__
    * Retornamos un __pad__ con el metodo __substring__
      * El metodo substring extrae caracteres segun los parametros
      * El primer parametro es de donde inciara a extraer
      * El segundo es donde terminara
    * Y le sumamos el valor del parametro __number__ 
    * Intento de explicacion
      * pad ='00', Number='1'
      * (padlength = 2) - (number.length = 1) = 1
      * pad.substring(0, 1) = "0" + number = 1
      * resultado'01'
  * Decalaramos la arrow function __formattedTime__
    * Recibe los segundos como parametro
    * Declaramos dos constantes
      * minutes la cual utiliza parseInt para parsear los segundos a minutos
      * seconds utiliza parseInt para parsear los segundos restantes de los minutos
    * La funcion parseInt parsea un string a un entero
      * En el primer parametro pasamos el numero a parsear
      * En el segundo parametro indicamos que tipo de sistema numeral sera usada en este caso son decimales

## __Barra de progreso__
* En el componente __video-player.js__
  * Importamos el componente __ProgressBar__
  * En las funciones __handleLoadedMetadata__ y __handleTimeUpdate__ quitamos el la funcion formattedTime ya que no funciona la barra de progreso
  * Creamos la funcion __handleProgressChange__, la cual se encargara de pasar el tiempo en el lugar donde presionemos en la barra de progreso o cuando movamos el indicador
  * En el componente __Timer__
    * Pasamos el __formattedTime__ a los atributos __duration__ y __currentTime__
  * Llamamos al componente __ProgressBar__
    * Pasamos los atributos __duration__ y __value__ con sus respectivos states
    * Pasamos el atributo __handleProgressChange__ el cual sera igual a la funcion del mismo nombre handleProgressChange
* Creamos el componente __progress-bar.js__
  * Importamos los estilos
  * Creamos una arrow function __ProgressBar__ 
    * Recibe las props
    * Retorna una etiqueta con la clase __ProgressBar__
    * Un input que recibe sus atributos
      * __type__ para el tipo de input que sera (range)
      * __min__ para el valor minimo del range
      * __max__ el valor maximo del range
      * __value__ el valor que tenga en cada momento que pase del video el indicador que va sobre la barra de progreso
      * __onChange__ el manejador de eventos, que se encarga de pasar los valores cuando se mueva el indicador eo se haga click en la barra de progreso

## __Spinner__
* En el componente __video-player.js__
  * Importamos el componente __Spinner__
  * Declaramos el state __loading__ con valor igual a false
  * Creamos dos funciones:
    * __handleSeeking__ cuando movamos la barra de progreso cambiara el state loading a true
    * __handleSeeked__ cuando terminae de cargar el video de donde manipulamos la barra de progreso, devolvera el state loading a false
  * Llamamos al componente __Spinner__ 
    * Le pasamos la propiedad active, la cual tendra el valor del this.state.loading
  * En el componente __Video__
    * Pasamos las propiedades __handleSeeking__ y __handleSeeked__ las cuales tienen el valor de las funciones con sus respectivos nombres
* En el componente __video.js__
  * Dentro de la constante declaramos las dos nuevas propiedaes __handleSeeking__ y __handleSeeked__ (DESTRUCTURING)
  * En la etiqueta __video__
    * Declaramos dos nuevos __Media Events__ onSeeking y onSeeked, los cuales al detectar una busqueda en el tiempo de progreso del video y al terminar de hacer la busqueda del nuevo tiempo en el video, llaman a las propiedades handleSeeking, handleSeeked respectivamente
* Creamos el componente __spinner.js__
  * Importamos estilos
  * Creamos la arrow function __Spinner__ 
    * Le pasamos las props
    * Preguntamos con una condicional __if__, si la propiedad active es __false__ entonces no hagas nada
    * Si la propiedad active es __true__ retornamos una etiqueta con la clase Spinner que muestra una etiqueta span

## __Control de volumen__
* En el componenten __video-player.js__
  * Importamos el componente __Volumen__
  * Creamos la funcion __handleVolumenChange__, la cual se encargara de setear el valor del volumnen, segun lo seleccionado en la barara de progreso, y lo asignara a la variable de entorno vide que tiene su elemento volumen (this.video.volumne)
  * Llamamos al componente __Volumen__ y le pasamos la propiedad __handleVolumenChange__ que es igual a la funcion con el mismo nombre
* Creamos el componente __volumen.js__
  * Importamos el componente  __VolumenIcon__
  * Improtamos estilos
  * Creamos una arrow function llamada Volumen
    * Le pasamos props
    * Creamos lasa etiquetes con sus respectivas clases y atributos
    * En la etiqueta __input__ el atributo steps sirve para indicar la cantidad de 'pasos' o divisiones que tendra la barra de rango del volumen y el onChange es el manejador de eventos que recibe por props la funcion que ajustar el rango del volumen segun sea manipulado
* Se hace reto
* Se hizo el reto de que cuando se presione el boton de la bocina se quede sin sonido y cuando se presione otra vez regrese al nivel de sonido que tenia

## __Botón de fullscreen__
* En el componente __video-player.js__
  * Importamos el componente __FullScreen__
  * Creamos la funcion __handleFullScreenClick__ que recibe un evento
    * Agregamos una condicional if que verifica si el document(pagina) __NO__ esta en fullscreen, entonces utilizamos la funcion __webkitRequestFullscreen__ de la api de JS. Esta funcion es parte del element del video(element.webkitRequestFullscreen()), el cual seteamos en la funcion __setRef__ en la varaible __this.player__. Esto para que el reproductor se vea en pantalla completa.
    * OJO --- La funcion ref se utiliza cuando queremos pasar el element (this.player = element) de algo a otros componentes que no pueden acceder a el
    * De lo contrario, que el documento(pagina) __SI__ este en pantalla completa entonces salir de esta __webkitExitFullscreen__ y regresar a la vista normal
  * Creamos la funcion __setRef__, la cual recibe un element como argumento, en este caso este element sera igual al element que retorna el __VideoPlayerLayout__ y lo almacenamos en la variabale __this.player__
  * En el componente __VideoPlayerLayout__
    * Pasamos la propiedad __setRef__ que sera igual a la funcion que lleva su mismo nombre __this.setRef__
  * En el componente __Fullscreen__ pasamos __handleFullScreenClick__ como propiedad que sera igual a la funcion con su mismo nombre __this.handleFullScreenClick__
* En el componente __video-player-layout.js__
  * En la etiqueta con el className __VideoPlayer__ agregamos el atributo __ref__ el cual recibe como propiedad la funcion __setRef__ del componente __video-player.js__

## __Puliendo detalles__
* En el componente __page/containers/home.js__
  * Modificamos la funcion __handleOpenModal__ ahora recive media como argumento
  * Seteamos el state media igual a la media
  * Movimos el componente __VideoPlayer__ dentro del componente __Modal__ y le agregamos propiedades src, title que reciben sus valores gracias a la media que declaramos arriba
* En el componente __media.js__
  * Creamos la funcion __handleClick__
    * Recibe event
    * Devuelve el __openModal__ de las props y le pasa como argumento las props. Estas props son la media del state del componente anterior
    * Modificamos el evento __onClick__ para que utilize la funcion __handleClick__
* En el componente __video-player.js__
  * Cambiamos el componente __Title__ para que reciba sus valore sde forma dinamica
* En el componente __Video__
  * Cambiamos el __src__ para que reciba su valor de forma dinamica
* En el componente __playlist.js__
  * Cambiamos el nombre del atributo __handleClick__ a __openModal__
* En el componente __modal.js__
  * En el __button__ agregamos la className
  * Eliminamos texto "cerrar"
